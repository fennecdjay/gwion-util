#ifndef __ERRMSG
#define __ERRMSG

#include <stdarg.h>
void gw_err(const char* fmt, ...);
void gw_out(const char* fmt, ...);
#endif
